<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Http\Controllers\TimepunchesController;
use App\Models\User;

class TimepunchesTest extends TestCase
{

    protected $timepunch;

    protected function setUp()
    {
        parent::setUp();

        $this->timepunch = new TimepunchesController();
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetTimepunchesByUser()
    {
        $valid_user_id = 517147;
        $valid_location_of_user_id = 25753;

        $timepunches = $this->timepunch->getTimepunchesByUser($this->fakeUserWithId($valid_user_id));

        $this->assertArrayHasKey('id', $timepunches[0]);
        $this->assertEquals($timepunches[0]['locationId'], $valid_location_of_user_id);
    }

    private function fakeUserWithId($userId) : User {
        $user = new User();
        $user->id = $userId;
        return $user;
    }
}
