<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RequestDataTest extends TestCase
{

    protected $testEndpoint = 'locations.json';

    public function testRequest()
    {

        $response = $this->get(config('app.BASE_API_REQUEST') . $this->testEndpoint);
        $response->assertStatus(200);

        $requestData = resolve('App\Library\Services\RequestData');
        $response = $requestData->get($this->testEndpoint);

        $this->assertEquals($response[25753]['address'], '7shifts Brazil');
        
    }

}
