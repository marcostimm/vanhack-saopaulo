<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Http\Controllers\UserController;

class UserTest extends TestCase
{
    protected $user;

    protected function setUp()
    {
        parent::setUp();

        $this->user = new UserController();
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetUserByLocationId()
    {
        $location_id = 25753;

        $userData = ($this->user)->getUserByLocationId($location_id);
        $arrUserData  = json_decode($userData, true);
        $this->assertEquals($arrUserData[0]["location_id"], $location_id);
        
    }
}
