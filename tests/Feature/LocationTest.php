<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Http\Controllers\LocationController;
use App\Models\Location;

class LocationTest extends TestCase
{

    protected $location;

    protected function setUp()
    {
        parent::setUp();

        $this->location = new LocationController();
    }

    /**
     * Test Locations List
     *
     * @return void
     */
    public function testLocationsList()
    {
        $locations = $this->location->getLocations();

        $this->assertArrayHasKey('id', $locations[0]);
    }

    /**
     * Test Locations List
     *
     * @return void
     */
    public function testLocationById()
    {
        $locationId = 25753;

        $location = ($this->location)->getLocation($locationId);
        
        $this->assertContainsOnlyInstancesOf(Location::class, [$location]);

        $this->assertEquals($location->id, $locationId);
    }

}
