<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/test', function (Request $request) {
    return $request->user();
});


Route::prefix('fetch')->group(function () {

    Route::get('/locations',            'FetchController@getLocations');
    Route::get('/users',                'FetchController@getUsers');
    Route::get('/timepunches',          'FetchController@getTimePunches');

});

Route::get('/locations',                'LocationController@getLocations');
Route::get('/locations/{id}/users',     'UserController@getUserByLocationId');

Route::get('/users/{id}',               'UserController@index');