<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;



class FetchController extends Controller
{

    protected $requestData;

    public function __construct()
    {
        $this->requestData = resolve('App\Library\Services\RequestData');
    }

    /**
     * Method getLocations
     *
     * @return void return description
     * @throws exception 
     * @access public
     */
    public function getLocations() : array
    {
        return Cache::remember('locations', config('app.CACHE_TIME'), function() {
            return  $this->requestData->get(env('ENDPOINT_LOCATIONS'));
        });
    }

    /**
     * Method getUsers
     *
     * @return void return description
     * @throws exception 
     * @access public
     */
    public function getUsers()
    {
        return Cache::remember('locations', config('app.CACHE_TIME'), function() {
            return  $this->requestData->get(env('ENDPOINT_USERS'));
        });
    }

        /**
     * Method getTimePunches
     *
     * @return void return description
     * @throws exception 
     * @access public
     */
    public function getTimePunches()
    {
        return Cache::remember('locations', config('app.CACHE_TIME'), function() {
            return  $this->requestData->get(env('ENDPOINT_TIME_PUNCHES'));
        });
    }
}
