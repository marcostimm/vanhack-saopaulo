<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\FetchController;
use App\Models\User;
use App\Models\Location;

class UserController extends Controller
{

    /**
     * Method index
     *
     * @param int $user_id User ID 
     *
     * @return string return json information about the user
     * @throws exception 
     * @access public
     */
    public function index(int $user_id)
    {
        $user           = (new User())->getUserById($user_id);
        $timePunches    = $this->getUserTimePunches($user);

        // This method will return all user data in a jSon format for display in a page
    }

    /**
     * Method index
     *
     * @return array return array with users normalized
     * @throws exception 
     * @access private
     */
    private function getUsersByLocation(Location $location)
    {
        $fetch = new FetchController();
        $usersData = $fetch->getUsers();

        $users = [];

        foreach($usersData[$location->id] as $id => $data) {
            $user = [];
            $user['location_id']    = $location->id;
            $user['id']             = $id;
            $user['active']         = $data['active'];
            $user['firstName']      = $data['firstName'];
            $user['lastName']       = $data['lastName'];
            $user['photo']          = $data['photo'];
            $user['userType']       = $data['userType'];

            $users[] = $user;
        }

        return json_encode($users);
    }

    /**
     * Method getUserByLocationId
     *
     * @param integer $location_id Id of a location
     *
     * @return string return json string with users by a location
     * @throws exception 
     * @access public
     */
    public function getUserByLocationId(int $location_id)
    {
        $location = (new LocationController())->getLocation($location_id);   
        $users = $this->getUsersByLocation($location);

        return $users;
    }

    /**
     * Method getUserTimePunches
     *
     * @param User $user User object
     *
     * @return void return description
     * @throws exception 
     * @access public
     */
    public function getUserTimePunches(User $user)
    {
        
        return (new TimepunchesController())->getTimepunchesByUser($user);
    }
}
