<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\FetchController;
use App\Models\Location;

class LocationController extends Controller
{
    /**
     * Method index
     *
     * @return array return normalized location data 
     * @throws exception 
     * @access public
     */
    public function getLocations() : array
    {
        $locationsData = (new Location())->loadLocations();

        $locations = [];

        foreach($locationsData as $id => $data) {
            $location = [];
            $location['id']                               = $id;
            $location['address']                          = $data['address'];
            $location['dailyOvertimeMultiplier']          = $data['labourSettings']['dailyOvertimeMultiplier'];
            $location['dailyOvertimeThreshold']           = $data['labourSettings']['dailyOvertimeThreshold'];
            $location['overtime']                         = $data['labourSettings']['overtime'];
            $location['weeklyOvertimeMultiplier']         = $data['labourSettings']['weeklyOvertimeMultiplier'];
            $location['weeklyOvertimeThreshold']          = $data['labourSettings']['weeklyOvertimeThreshold'];

            $locations[] = $location;
        }

        return $locations;
    }

    /**
     * Method getLocation
     *
     * @param integer $location_id Integer of a locataion
     *
     * @return Location return object Location
     * @throws exception 
     * @access public
     */
    public function getLocation(int $location_id) : Location
    {
        $locations = (new Location())->loadLocations();
        if (!$locations[$location_id]) {
            return $response->json([]);
        }

        $location = new Location();
        $location->id                           = $locations[$location_id]['id'];
        $location->address                      = $locations[$location_id]['address'];
        $location->dailyOvertimeMultiplier      = $locations[$location_id]['labourSettings']['dailyOvertimeMultiplier'];
        $location->dailyOvertimeThreshold       = $locations[$location_id]['labourSettings']['dailyOvertimeThreshold'];
        $location->overtime                     = $locations[$location_id]['labourSettings']['overtime'];
        $location->weeklyOvertimeMultiplier     = $locations[$location_id]['labourSettings']['weeklyOvertimeMultiplier'];
        $location->weeklyOvertimeThreshold      = $locations[$location_id]['labourSettings']['weeklyOvertimeThreshold'];
        
        return $location;
    }



}
