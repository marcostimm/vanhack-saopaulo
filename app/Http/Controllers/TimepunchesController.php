<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Timepunch;
use App\Models\User;

class TimepunchesController extends Controller
{
    /**
     * Method getTimepunchesByUser
     *
     * @param User $user User object
     *
     * @return void return description
     * @throws exception 
     * @access public
     */
    public function getTimepunchesByUser(User $user)
    {
        $timePunchesData = (new Timepunch())->loadTimepunches();

        $timePunches = [];

        foreach($timePunchesData as $timePunch) {
            if($timePunch['userId'] == $user->id) {
                $time                   = [];
                $time['id']             = $timePunch['id'];
                $time['clockedIn']      = $timePunch['clockedIn'];
                $time['clockedOut']     = $timePunch['clockedOut'];
                $time['hourlyWage']     = $timePunch['hourlyWage'];
                $time['locationId']     = $timePunch['locationId'];

                $timePunches[] = $time;
            }
        }

        return $timePunches;
    }
}
