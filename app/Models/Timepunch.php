<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\FetchController;

class Timepunch extends Model
{
    /**
     * Method loadTimepunches
     *
     * @return void return description
     * @throws exception 
     * @access public
     */
    public function loadTimepunches()
    {
        $fetch = new FetchController();
        return $fetch->getTimePunches();
    }

}
