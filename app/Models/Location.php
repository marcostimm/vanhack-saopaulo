<?php

namespace App\Models;

use App\Models\Model;
use App\Http\Controllers\FetchController;

class Location extends Model
{
    protected $id;
    protected $address;
    protected $dailyOvertimeMultiplier;
    protected $dailyOvertimeThreshold;
    protected $overtime;
    protected $weeklyOvertimeMultiplier;
    protected $weeklyOvertimeThreshold;

    /**
     * Method loadLocations
     *
     * @return array return locations
     * @throws exception 
     * @access public
     */
    public function loadLocations()
    {
        $fetch = new FetchController();
        return $fetch->getLocations();
    }
    
}