<?php

namespace App\Models;

use App\Models\Model;
use App\Http\Controllers\FetchController;

class User extends Model
{
    protected $id;
    protected $location_id;
    protected $active;
    protected $firstName;
    protected $lastName;
    protected $photo;
    protected $userType;

    /**
     * Method __construct
     *
     * @return void return description
     * @throws exception 
     * @access public
     */
    public function __construct() {
        // 
    }

    /**
     * Method getUserById
     *
     * @param int $user_id Id of user
     *
     * @return User return User object
     * @throws exception 
     * @access public
     */
    public function getUserById(int $user_id)
    {

        $locationUsers = $this->loadUsers();

        foreach($locationUsers as $LocationKey => $users) {
            foreach($users as $key => $user) {
                if ($key == $user_id) {
                    
                    $foundedUser                   = new User();
                    $foundedUser->id               = $user['id'];
                    $foundedUser->location_id      = $user['locationId'];
                    $foundedUser->active           = $user['active'];
                    $foundedUser->firstName        = $user['firstName'];
                    $foundedUser->lastName         = $user['lastName'];
                    $foundedUser->photo            = $user['photo'];
                    $foundedUser->userType         = $user['userType'];

                    return $foundedUser;
                }
            }
        }
        return null;
    }

    /**
     * Method loadUsers
     *
     * @return array return users array
     * @throws exception 
     * @access public
     */
    public function loadUsers()
    {
        $fetch = new FetchController();
        return $fetch->getUsers();
    }

    /**
     * Method weeklyOvertime
     *
     * @param User $user user object
     *
     * @return string return user informations
     * @throws exception 
     * @access public
     */
    public function weeklyOvertime(User $user)
    {
        $timePunch = new TimePunchesController($user);
        
    }
}
