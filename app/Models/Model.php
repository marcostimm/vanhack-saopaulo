<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as EloquentModel;

class Model
{
    public function __get($name)
    {
        if (property_exists($this, $name)) {
            return $this->$name;
        }
    }

    public function __set($name, $value)
    {
        if (property_exists($this, $name)) {
            return $this->{$name} = $value;
        }
    }

    public function __isset($name)
    {
        if (property_exists($this, $name)) {
            return true;
        }
        return false;
    }
}
