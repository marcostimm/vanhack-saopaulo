<?php

namespace App\Library\Services;

/**
 * RequestDataInterface Interface
 *
 * Contract for RequestData
 *
 * @category /Users/timm/Documents/Personal/www/vanhack/saopaulofair/app/Library/Services
 * @package  
 * @author   Marcos Timm <timm@marcos.im>
 * @license  Copyright (C)
 * @link     https://marcos.im
 */
interface RequestDataInterface
{
    public function get(string $endpoint) : array;       
}