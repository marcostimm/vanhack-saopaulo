<?php

namespace App\Library\Services;

use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;
use App\Library\Services\RequestDataInterface;

/**
 * RequestData Class
 *
 * Request API layer from requests
 *
 * @category Library/Services
 * @package  VanHack
 * @author   Marcos Timm <timm@marcos.im>
 * @license  Copyright (C)
 * @link     https://marcos.im
 */
class RequestData extends ServiceProvider implements RequestDataInterface
{

    protected $client;

    protected $endpoint;

    protected $response = null;

    /**
     * Method __contruct
     *
     * @param string endpoint URL for endpoint
     *
     * @return stirng return a json string with the response body of request
     * @throws exception 
     * @access public
     */
    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => config('app.BASE_API_REQUEST'),
            'timeout'  => 300,
        ]);
        // $this->endpoint = $endpoint;
    }

    /**
     * Method get
     *
     * @param string param description
     *
     * @return void return description
     * @throws exception 
     * @access public
     */
    public function get(string $endpoint) : array
    {
        $this->endpoint = $endpoint;

        // Try to request the API endpoint
        try {
            $response = $this->client->get($this->endpoint);
            if ($response->getStatusCode() != 200) {
                throw new Exception('Http request error');
            }
            $this->response = $response;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
        
        // If everything goes well, return the response body with json decoded
        return json_decode($this->response->getBody(), true);
    }
}