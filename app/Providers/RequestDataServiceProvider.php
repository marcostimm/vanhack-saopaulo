<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Library\Services\RequestData;

class RequestDataServiceProvider extends ServiceProvider
{

    protected $defer = true;

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        \App::singleton('App\Library\Services\RequestData', function () {
            return new \App\Library\Services\RequestData();
        });
    }
}
