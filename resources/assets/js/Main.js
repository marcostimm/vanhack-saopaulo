import React, { Component } from 'react';
import { render } from 'react-dom';
import { Route, Switch } from 'react-router-dom'
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import rootReducer from './rootReducer';
import { BrowserRouter, Router } from 'react-router-dom'

import Locations from './components/Locations';

const store = createStore(
    rootReducer,
    compose(
      applyMiddleware(thunk),
      window.devToolsExtension ? window.devToolsExtension() : f => f
    )
);

render(
    <Provider store={store}>
            <BrowserRouter>
            <Switch>
    <div className="container">
    <h1>Hello São Paulo Recruiting Fair</h1>
    <Locations />
    </div>            </Switch>
        </BrowserRouter>
    </Provider>, document.getElementById('root'));