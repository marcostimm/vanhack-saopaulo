import { combineReducers } from 'redux';

import locations from './reducers/locations';

export default combineReducers({
  locations
});