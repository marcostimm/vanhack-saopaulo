import axios from 'axios';
import jwtDecode from 'jwt-decode';
import { GET_LOCATIONS } from './types';

export function getLocations(locations, isLoaded) {
  return {
    type: GET_LOCATIONS,
    locations,
    isLoaded
  };
}

export function locationsList() {
  return dispatch => {
    return axios.get('/api/locations').then(res => {
      dispatch(getLocations(res.data, true));
    });
  }
};