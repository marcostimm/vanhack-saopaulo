import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { locationsList }    from '../actions/locationActions';

class Locations extends Component {
 
    constructor(props) {
        super(props);

        this.state = {
          errors:           {},
          isLoading:        true,
          locations:    []
        };

        this.loadLocations()
    }

    loadLocations() {
        this.props.locationsList().then(
            (res) => {this.setState({locations: this.props.locations.locations}) },
            (err) => this.setState({ errors: err.response, isLoading: false })
        );
    }

    render() {

        const { locations } = this.state;

        return(  
            <div className="row">
                <div className="col-4">
                    <div className="list-group" id="list-tab" role="tablist">
                    { this.props.locations ? (locations.map((location, i) => <a className="list-group-item list-group-item-action" id="locations-list-{i}" key={i} data-toggle="list" href="#list-home" role="tab" aria-controls="location-{location.id}">{location.address}</a>)) : <span>Carregando...</span> }
                    </div>
                </div>
                <div className="col-8">
                    <div className="tab-content" id="nav-tabContent">
                    <div className="tab-pane fade show active" id="list-home" role="tabpanel" aria-labelledby="list-home-list">...</div>
                    <div className="tab-pane fade" id="list-profile" role="tabpanel" aria-labelledby="list-profile-list">...</div>
                    <div className="tab-pane fade" id="list-messages" role="tabpanel" aria-labelledby="list-messages-list">...</div>
                    <div className="tab-pane fade" id="list-settings" role="tabpanel" aria-labelledby="list-settings-list">...</div>
                    </div>
                </div>
            </div>
        )
    }
}

Locations.propTypes = {
    locationsList: PropTypes.func.isRequired
}

const mapStateToProps=(state)=>{
    return state
}

export default connect(mapStateToProps, { locationsList } )(Locations);